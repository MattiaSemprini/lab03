package it.unibo.oop.lab03.encapsulation;

public final class TestSimpleBankAccount {

    private TestSimpleBankAccount() { }

    public static void main(final String[] args) {
        /*
         * 1) Creare l' AccountHolder relativo a Mario Rossi con id 1 
         * 2) Creare
         * 		l' AccountHolder relativo a Luigi Bianchi con id 2 
         * 3) Creare i due
         * 		SimpleBankAccount corrispondenti 
         * 4) Effettuare una serie di depositi e
         * 		prelievi 
         * 5) Stampare a video l'ammontare dei due conti e verificare
         * 		la correttezza del risultato 
         * 6) Provare a prelevare fornendo un idUsr
         *		sbagliato 
         * 7) Controllare nuovamente l'ammontare
         */
	    
    	AccountHolder mr = new AccountHolder("Mario", "Rossi", 1);
    	AccountHolder lb = new AccountHolder("Luigi", "Bianchi", 1);
    	
    		SimpleBankAccount a = new SimpleBankAccount(mr.getUserID(), 10.0);
	    	SimpleBankAccount b = new SimpleBankAccount(lb.getUserID(), 10000.0);
	    	
	    	System.out.println(a.getBalance());
	    	System.out.println(b.getBalance());
	    	
	    	a.deposit(mr.getUserID(), 1000.0);
	    	a.withdraw(mr.getUserID() , 1011.0);
	    	
	    	b.depositFromATM(lb.getUserID(), 100000.0);
	    	
	    	System.out.println(a.getBalance());
	    	System.out.println(b.getBalance());
	    	
	    	System.out.println(a.getnTransictions());
	    	
	    	a.withdraw(lb.getUserID(), 9999);
	    	
	    	System.out.println(a.getBalance());
	    	
	    	
	    	
	    	
	    
    
    }
    
}
