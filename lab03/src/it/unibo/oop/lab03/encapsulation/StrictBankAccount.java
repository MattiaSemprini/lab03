package it.unibo.oop.lab03.encapsulation;

import it.unibo.oop.lab03.interfaces.BankAccount;

public class StrictBankAccount implements BankAccount{

	@Override
	public void withdraw(int usrID, double amount) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deposit(int usrID, double amount) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void depositFromATM(int usrID, double amount) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void withdrawFromATM(int usrID, double amount) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void computeManagementFees(int usrID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double getBalance() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getNTransactions() {
		// TODO Auto-generated method stub
		return 0;
	}

}
