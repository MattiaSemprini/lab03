package it.unibo.oop.lab03.encapsulation;

import it.unibo.oop.lab03.interfaces.BankAccount;

public class SimpleBankAccount implements BankAccount{

     /*
     * Aggiungere i seguenti campi:
     * - double balace: ammontare del conto
     * - int userID: id del possessore del conto
     * - int nTransactions: numero delle operazioni effettuate
     * - static double ATM_TRANSACTION_FEE = 1: costo delle operazioni via ATM
     */
    
    private double balance=0;
    private int userID;
    private int nTransictions;
    private static double ATM_TRANSACTION_FEE = 1;

    /*
     * Creare un costruttore pubblico che prenda in ingresso un intero (ossia l'id
     * dell'utente) ed un double (ossia, l'ammontare iniziale del conto corrente).
     */

    /*
     * Si aggiungano selettori per: 
     * - ottenere l'id utente del possessore del conto
     * - ottenere il numero di transazioni effettuate
     * - ottenere l'ammontare corrente del conto.
     */
    
    public SimpleBankAccount(int id, double ammount) {
    	
    	setBalance(ammount);
    	setUserID(id);
    }

    public void deposit(final int usrID, final double amount) {
        /*
         * Incrementa il numero di transazioni e aggiunge amount al totale del
         * conto Nota: il deposito va a buon fine solo se l'id utente
         * corrisponde
         */
    	if (checkUser(usrID)) {
    	
    	setnTransictions(nTransictions + 1);
    	setBalance(amount + balance );
    	
    	}
    	
    }

    public void withdraw(final int usrID, final double amount) {
        /*
         * Incrementa il numero di transazioni e rimuove amount al totale del
         * conto. Note: - Il conto puo' andare in rosso (ammontare negativo) -
         * Il prelievo va a buon fine solo se l'id utente corrisponde
         */
    	if (checkUser(usrID)) {
    		setnTransictions(getnTransictions() + 1 );
    		setBalance(getBalance() - amount);
    	}
    }

    public void depositFromATM(final int usrID, final double amount) {
        /*
         * Incrementa il numero di transazioni e aggiunge amount al totale del
         * conto detraendo le spese (costante ATM_TRANSACTION_FEE) relative
         * all'uso dell'ATM (bancomat) Nota: il deposito va a buon fine solo se
         * l'id utente corrisponde
         */
    	if (checkUser(usrID)) {
    		
    		setnTransictions(nTransictions + 1);
        	setBalance(amount + balance - ATM_TRANSACTION_FEE);	
    	}
    }

    public void withdrawFromATM(final int usrID, final double amount) {
        /*
         * Incrementa il numero di transazioni e rimuove amount + le spese
         * (costante ATM_TRANSACTION_FEE) relative all'uso dell'ATM (bancomat)
         * al totale del conto. Note: - Il conto puo' andare in rosso (ammontare
         * negativo) - Il prelievo va a buon fine solo se l'id utente
         * corrisponde
         */
    	if(checkUser(usrID)== true) {
    	setnTransictions(nTransictions + 1);
    	setBalance(balance - (amount + ATM_TRANSACTION_FEE));
    	}
    }

    /* Utility method per controllare lo user */
    private boolean checkUser(final int id) {
        return this.userID == id;
    }

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public int getnTransictions() {
		return nTransictions;
	}

	public void setnTransictions(int nTransictions) {
		this.nTransictions = nTransictions;
	}

	@Override
	public void computeManagementFees(int usrID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getNTransactions() {
		// TODO Auto-generated method stub
		return 0;
	}
}
