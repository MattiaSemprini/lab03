package it.unibo.oop.lab03.acme;

import java.util.Arrays;

public class Exam {
	
	int id;
	int maxStudent;
	int registeredStudents;
	String courseName;
	Professor professor;
	ExamRoom room;
	Student[] students;
	
	public Exam(int id,int max,int regstud, String coursename, Professor prof, ExamRoom room) {
		
		this.id = id;
		this.maxStudent=max;
		this.registeredStudents = regstud;
		this.courseName = coursename;
		this.professor = prof;
		this.room = room;
		this.students = new Student[10];
	}
	
	public void registerStudent(Student student) {
		if(student != null) {
			this.students[registeredStudents] = student;
			registeredStudents++;
		}
	}
	
	public String toString() {
		
		return " id="+this.id+
				" Studenti totali:"+ this.registeredStudents+
				" corso:"+this.courseName+
				" Professore:"+ this.professor.surname+
				" Aula"+this.room+
				" Studenti:"+ Arrays.toString(this.students);
		
	}
	
	
	
	public void setId(int id) {
		this.id = id;
	}

	public void setRegisteredStudents(int registeredStudents) {
		this.registeredStudents = registeredStudents;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public void setRoom(ExamRoom room) {
		this.room = room;
	}


	
	
}
