package it.unibo.oop.lab03.acme;

public final class Testing {

    private Testing() { }

    public static void main(final String[] args) {
        /*
         * 1) Creare 3 studenti a piacere 2) Creare 2 docenti a piacere 3)
         * Creare due aulee di esame, una con 100 posti una con 80 posti 4)
         * Creare due esami, uno con nMaxStudents=10, l'altro con nMaxStudents=2
         * 5) Iscrivere tutti e 3 gli studenti agli esami 6) Stampare in stdout
         * la rapresentazione in stringa dei due esami
         */
    	String[] corsi = new String[10];
    	corsi[0]="mdp";
    	corsi[1]="oop";
    	
    	String[] corsi2 = new String[10];
    	corsi2[4]="oop";
    	
    	
    	Student rossi = new Student(1,"gigetto","rossi","500", 2017);
    	Student neri = new Student(1,"giorgio","neri","5500", 2018);
    	Student gialli = new Student(1,"gigino","gialli","700", 2017);
    	
    	Professor gino = new Professor(2,"gino","nero","12345");
    	Professor Viroli = new Professor(2,"mirko","Viroli","12345");
    	
    	ExamRoom a = new ExamRoom(100,"lab vela",true,true);
    	ExamRoom b = new ExamRoom(80,"lab velo",true,false);
    	
    	Exam oop = new Exam(1,10,0,"oop",Viroli,a);
    	Exam fdp = new Exam(1,10,0,"fdp",Viroli,a);

    	oop.registerStudent(rossi);
    	oop.registerStudent(neri);
    	oop.registerStudent(gialli);
    	
    	fdp.registerStudent(gialli);
    	fdp.registerStudent(rossi);
    	fdp.registerStudent(neri);

    	System.out.println(a.toString());
    	System.out.println(b.toString());



    
    	gino.courses = corsi;
    	gino.replaceAllCourses(corsi2);
    }
}
