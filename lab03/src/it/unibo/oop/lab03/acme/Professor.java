package it.unibo.oop.lab03.acme;

public class Professor implements User {

	int id;
	String name;
	String surname;
	String password;
	String[] courses;
	
	public Professor(int id , String name,String surname,String password) {
		
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.password = password;
	}
	
	@Override
	public String getUsername() {
		
		return this.name;
	}

	@Override
	public String getPassword() {
		
		return this.password;
	}

	@Override
	public String getDescription() {
		return "Professor [name=" + this.name
        + ", surname=" + this.surname
        + ", id=" + this.id + "]";
	}
	
	public void replaceCourse(String course, int index) {
	
		if(course != null) {
		courses[index] = course;
		}
	}
	
	public void replaceAllCourses(String[] courses) {
		
		if(courses != null) {
			this.courses = courses;
		}
	}

}
